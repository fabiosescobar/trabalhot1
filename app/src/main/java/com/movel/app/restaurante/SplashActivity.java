package com.movel.app.restaurante;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



   // private void fazerSplash() {
   //     Handler handle = new Handler();
       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() { telaPrincipal(); }
        }, 3000);
    }

    private void telaPrincipal() {

        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
        finish();
    }
}
